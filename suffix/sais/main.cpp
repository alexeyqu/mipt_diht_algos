#include "sais.h"

int main()
{
  int key = 0;
  std::string str;

  std::cin >> key >> str;

  int problemSize = str.size();

  str += str;

  std::vector <long long> ans;

  ans.resize (problemSize);

  for (size_t i = 0; i < problemSize; i++)
  {
    SuffixArrayInducedSorting sais;

    std::string substring = str.substr (i, key);

    // std::cout << '\'' << substring << '\'' << std::endl;

    std::vector <Index_t> suffixArray;
    sais.getSuffixArray (substring, suffixArray);

    substring += '^';

    std::vector <Index_t> lcpArray;
    getLcpArrayKasai (substring, suffixArray, lcpArray);

    for (size_t j = 0; j < key; j++)
    {
      ans[i] += key - suffixArray[j + 1] - lcpArray[j];
    }
  }

  for (size_t i = 0; i < problemSize; i++)
  {
    std::cout << ans[i] << ' ';
  }

  return 0;
}