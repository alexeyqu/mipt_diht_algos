#include "esko.h"

//-----------------------------------------------------------------------------
//-----NODE--------------------------------------------------------------------
//-----------------------------------------------------------------------------

SuffixTree::Node::Node (Index_t *lastPosition, int parent, Index_t indexNode) : \
  startPosition_ (*lastPosition - 1), lastCharPointer_ (lastPosition), parent_ (parent), indexNode_ (indexNode), isLeaf_ (true)
{
  for (int i = 0; i < ALPHABET_SIZE; i++)
  {
    go_[i] = -1;
  }
}

SuffixTree::Node::Node (Index_t startPosition, int parent, Index_t indexNode, Index_t length) : \
  startPosition_ (startPosition), parent_ (parent), indexNode_ (indexNode), length_ (length), isLeaf_ (false)
{
  for (int i = 0; i < ALPHABET_SIZE; i++)
  {
    go_[i] = -1;
  }
}

int SuffixTree::Node::getNextNode (char c)
{
  return go_[c];
}

void SuffixTree::Node::setNextNode (char c, int node)
{
  go_[c] = node;
}

Index_t SuffixTree::Node::getDistance()
{
  return (isLeaf_? (*lastCharPointer_ - startPosition_) : length_);
}

//-----------------------------------------------------------------------------
//-----UKKONEN-----------------------------------------------------------------
//-----------------------------------------------------------------------------

SuffixTree::Ukkonen::Ukkonen (std::string &str, SuffixTree &suffTree) : \
  str_ (str), suffTree_ (suffTree), treeRef_ (suffTree.tree_)
{
  Node empty_ = Node (0, EMPTY_INDEX, EMPTY_INDEX, 0);
  Node root_ = Node (0, EMPTY_INDEX, ROOT_INDEX, 1);

  treeRef_.push_back (empty_);
  treeRef_.push_back (root_);

  for (Index_t c = 0; c < ALPHABET_SIZE; c++)
  {
    treeRef_[EMPTY_INDEX].setNextNode (c, ROOT_INDEX);
  }

  treeRef_[ROOT_INDEX].setSuffixLink (EMPTY_INDEX);

  curOffset_ = 1;
  curPoint_ = 1;
  prevLinkPoint_ = 1;
  suffTree_.lastPosition_ = 0;
}

void SuffixTree::Ukkonen::appendChar (char c)
{
  lastChar_ = c;
  suffTree_.lastPosition_++;

  isEmptyPhase_ = false;

  while (1)
  {
    adjustOffset();
    walkTree();

    if (curLinkPoint_ &&
      prevLinkPoint_ != 1)
    {
      treeRef_[prevLinkPoint_].setSuffixLink (curLinkPoint_);
      prevLinkPoint_ = 1;
    }

    if (isEmptyPhase_) break;

    prevLinkPoint_ = curLinkPoint_;
    curLinkPoint_ = 0;
    curPoint_ = treeRef_[treeRef_[curPoint_].getParent()].getSuffixLink();

    if (curOffset_)
    {
      curPoint_ = treeRef_[curPoint_].getNextNode (str_[firstPosition_]);
    }
    else
    {
      curOffset_ = treeRef_[curPoint_].getLength();
    }
  }
}

void SuffixTree::Ukkonen::adjustOffset()
{
  while (curOffset_ > treeRef_[curPoint_].getDistance())
  {
    curOffset_ -= treeRef_[curPoint_].getDistance();
    firstPosition_ += treeRef_[curPoint_].getDistance();

    curPoint_ = treeRef_[curPoint_].getNextNode (str_[firstPosition_]);
  }
}

void SuffixTree::Ukkonen::walkTree()
{
  if (curOffset_ == treeRef_[curPoint_].getDistance())
  {
    if (treeRef_[curPoint_].getNextNode (lastChar_) == -1)
    {
      addLeaf();
      curOffset_ = 0;
    }
    else
    {
      curPoint_ = treeRef_[curPoint_].getNextNode (lastChar_);
      isEmptyPhase_ = true;
      curOffset_ = 1;
    }

    curLinkPoint_ = treeRef_[curPoint_].getParent();
  }
  else
  {
    if (str_[treeRef_[curPoint_].getStartPosition() + curOffset_] != lastChar_)
    {
      split();
    }
    else
    {
      curOffset_++;
      isEmptyPhase_ = true;
    }
  }
}

void SuffixTree::Ukkonen::addLeaf()
{
  Node newLeaf = Node (&suffTree_.lastPosition_, curPoint_, treeRef_.size());
  treeRef_.push_back (newLeaf);

  int newLeafIndex = (int) treeRef_.size() - 1;

  treeRef_[curPoint_].setNextNode (lastChar_, newLeafIndex);

  curPoint_ = newLeafIndex;
  firstPosition_ = treeRef_[curPoint_].getStartPosition();
}

void SuffixTree::Ukkonen::split()
{
  Node midVertex = Node (treeRef_[curPoint_].getStartPosition(), treeRef_[curPoint_].getParent(), treeRef_.size(), curOffset_);
  treeRef_.push_back (midVertex);

  int midVertexIndex = (int) treeRef_.size() - 1;

  treeRef_[curPoint_].setStartPosition (treeRef_[curPoint_].getStartPosition() + curOffset_);
  treeRef_[curPoint_].setLength (treeRef_[curPoint_].getLength() - curOffset_);
  treeRef_[curPoint_].setParent(midVertexIndex);

  Node newLeaf = Node (&suffTree_.lastPosition_, midVertexIndex, treeRef_.size());
  treeRef_.push_back (newLeaf);

  treeRef_[treeRef_[midVertexIndex].getParent()].setNextNode (str_[treeRef_[midVertexIndex].getStartPosition()], midVertexIndex);

  treeRef_[midVertexIndex].setNextNode (str_[treeRef_[curPoint_].getStartPosition()], curPoint_);
  treeRef_[midVertexIndex].setNextNode (str_[newLeaf.getStartPosition()], (int) newLeaf.getIndexNode());

  curLinkPoint_ = midVertexIndex;
  curPoint_ = midVertexIndex;
  firstPosition_ = treeRef_[midVertexIndex].getStartPosition();
}

void SuffixTree::Ukkonen::printSubtree (Node *node)
{
  std::cout << node->getIndexNode() << ": ";

  for (int c = 0; c < ALPHABET_SIZE; c++)
  {
    int nextNode = node->getNextNode (c);
          // std::cout << "DBG: " << (char) (c + 'a') << ' ' << node->getNextNode (c) << std::endl;
    if (nextNode >= 2)
    {
      std::string str = assembleEdge (str_, treeRef_[nextNode].getStartPosition(), treeRef_[nextNode].getDistance());
      std::cout << " -" << str << "->" << treeRef_[nextNode].getIndexNode() << ' ';
    }
  }

  // return;

  std::cout << std::endl;

  for (char c = 0; c < ALPHABET_SIZE; c++)
  {
    int nextNode = node->getNextNode (c);

    if (nextNode >= 2)
    {
      this->printSubtree (&treeRef_[nextNode]);
    }
  }
}

std::string SuffixTree::Ukkonen::assembleEdge (std::string &s, Index_t start, Index_t length)
{
  std::string str;

  for (Index_t i = 0; i < length; i++)
  {
    str += s[start + i] + 'a';
  }

  return str;
}

//-----------------------------------------------------------------------------
//-----SUFFIXTREE--------------------------------------------------------------
//-----------------------------------------------------------------------------

void SuffixTree::buildTree (std::string &str)
{
  Ukkonen builder (str, *this);

  for (Index_t i = 0; i < str.size(); i++)
  {
    builder.appendChar (str[i]);

#ifdef NOJUDGE
    builder.printSubtree (&tree_[ROOT_INDEX]);
#endif
  }
}

long long SuffixTree::calcSum()
{
  long long sum = 0;

  Node *cur = &tree_[ROOT_INDEX + 1];
  Node *last = &tree_[tree_.size() - 1] + 1;

  for ( ; cur != last; cur++)
  {
    sum += cur->getDistance();
  }

  return sum;
}