#include "esko.h"

int main() // this part was designed to solve acm.timus.ru #1706 problem
{
  int key = 0;
  std::string str;

  std::cin >> key >> str;

  int problemSize = str.size();

  for (size_t i = 0; i < problemSize; i++)
  {
    str[i] -= 'a';
  }

  str += str;

  std::vector <long long> ans;

  ans.resize (problemSize);

  for (size_t i = 0; i < problemSize; i++)
  {
    SuffixTree tree;

    std::string substring = str.substr (i, key);

    tree.buildTree (substring);

    ans[i] = tree.calcSum();
  }

  for (size_t i = 0; i < problemSize; i++)
  {
    std::cout << ans[i] << ' ';
  }

  return 0;
}