#ifndef _UKKONEN_LIB__
#define _UKKONEN_LIB__

#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <memory.h>
#include <vector>
#include <string>
#include <map>

#define ALPHABET_SIZE 26
#define EMPTY_INDEX 0
#define ROOT_INDEX 1

typedef uint32_t Index_t;

class SuffixTree
{
private:

  class Node
  {
  private:
    int go_[ALPHABET_SIZE];
    int parent_, suffixLink_;
    Index_t indexNode_, length_;
    Index_t startPosition_;
    Index_t *lastCharPointer_;
    bool isLeaf_;

  public:
    explicit Node (Index_t *lastPosition, int parent, Index_t indexNode);
    explicit Node (Index_t startPosition, int parent, Index_t indexNode, Index_t length);

    int getNextNode (char c);
    void setNextNode (char c, int node);

    int getParent() {return parent_;}
    void setParent (int parent) {parent_ = parent;}

    int getSuffixLink() {return suffixLink_;}
    void setSuffixLink (int suffixLink) {suffixLink_ = suffixLink;}

    Index_t getLength() {return length_;}
    void setLength (Index_t length) {length_ = length;}

    Index_t getStartPosition() {return startPosition_;}
    void setStartPosition (Index_t startPosition) {startPosition_ = startPosition;}

    Index_t getIndexNode() {return indexNode_;}

    Index_t getDistance();
  };

  class Ukkonen
  {
  private:
    Index_t firstPosition_, curOffset_;
    int curPoint_, curLinkPoint_, prevLinkPoint_;
    Index_t lastChar_;
    bool isEmptyPhase_;

    std::string &str_;
    SuffixTree &suffTree_;

    std::vector <Node> &treeRef_;

  public:
    explicit Ukkonen (std::string &str, SuffixTree &suffTree);

    void appendChar (char c);
    void walkTree();
    void addLeaf();
    void split();
    void adjustOffset();

    void printSubtree (Node *node);
    std::string assembleEdge (std::string &s, Index_t start, Index_t length);
  };

  std::vector <Node> tree_;
  Index_t lastPosition_;

public:
  Index_t root_, empty_;

  explicit SuffixTree() : lastPosition_ (0), root_ (0), empty_ (0) {};

  ~SuffixTree() {};

  void buildTree (std::string &str);

  long long calcSum();
};

#endif